/*
  PDE INSTITOTO SAN JOSE
  
  ArduinoMqttClient - WiFi Simple Sender

  Este ejemplo conecta al PDE con un Bloker MQTT
  y publica un mensaje de prueba

  Es válido para
  - Arduino MKR 1000, MKR 1010 or Uno WiFi Rev.2 board

*/

#include <ArduinoMqttClient.h>

#include <ESP8266WiFi.h>

#include "arduino_secrets.h"

/** A DEFINIR POR PROFESOR**/

#define SECRET_SSID "Telecentro-ddf4"
#define SECRET_PASS "KTWYMMWYMWN4"

#define TOPIC_TEMP "smartway/cliente1/edificio1/oficinax/temperatura"
#define TOPIC_HUME "smartway/cliente1/edificio1/oficinax/humedad"
#define TOPIC_PRES "smartway/cliente1/edificio1/oficinax/presion"

#define BROKER_IP "10.0.2.15"


char ssid[] = SECRET_SSID;        // your network SSID (name)
char pass[] = SECRET_PASS;    // your network password (use for WPA, or use as key for WEP)


WiFiClient wifiClient;
MqttClient mqttClient(wifiClient);

const char broker[] = BROKER_IP;
int        port     = 1883;

const long interval = 2000;
unsigned long previousMillis = 0;

int count = 0;

void setup() {
  //Inicializar serial y Esperar y esperar por el Puerto abiero
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }

  // attempt to connect to Wifi network:
  Serial.print("Attempting to connect to WPA SSID: ");
  Serial.println(ssid);
  while (WiFi.begin(ssid, pass) != WL_CONNECTED) {
    // failed, retry
    Serial.print(".");
    delay(5000);
  }

  Serial.println("You're connected to the network");
  Serial.println();

  // You can provide a unique client ID, if not set the library uses Arduino-millis()
  // Each client must have a unique client ID
  // mqttClient.setId("clientId");

  // You can provide a username and password for authentication
  // mqttClient.setUsernamePassword("username", "password");

  Serial.print("Attempting to connect to the MQTT broker: ");
  Serial.println(broker);

  if (!mqttClient.connect(broker, port)) {
    Serial.print("MQTT connection failed! Error code = ");
    Serial.println(mqttClient.connectError());

    while (1);
  }

  Serial.println("You're connected to the MQTT broker!");
  Serial.println();

  randomSeed(0);
}

void loop() {
  // call poll() regularly to allow the library to send MQTT keep alives which
  // avoids being disconnected by the broker
  mqttClient.poll();

  // avoid having delays in loop, we'll use the strategy from BlinkWithoutDelay
  // see: File -> Examples -> 02.Digital -> BlinkWithoutDelay for more info
  unsigned long currentMillis = millis();
  
  if (currentMillis - previousMillis >= interval) {
    // save the last time a message was sent
    previousMillis = currentMillis;

    Serial.print("Sending MQTT message ");

    
    // send message, the Print interface can be used to set the message contents
    mqttClient.beginMessage(TOPIC_TEMP);
    mqttClient.print(random(0,45));
    mqttClient.endMessage();
    delay(10);

    mqttClient.beginMessage(TOPIC_HUME);
    mqttClient.print(random(15,95));
    mqttClient.endMessage();
    delay(10);

    mqttClient.beginMessage(TOPIC_PRES);
    mqttClient.print(random(980,1040));
    mqttClient.endMessage();


    Serial.println();


  }
}
